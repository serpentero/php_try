<?php
class block_msktime extends block_base {
public function init() {
    $this->title = get_string('msktime', 'block_msktime');
}

public function get_content() {
if ($this->content !== null) {
  return $this->content;
}

$this->content         =  new stdClass;
$this->content->text   = "<h1>".date("H:i:s")."</h1>";
//$this->content->footer = 'Footer here...'; 

return $this->content;
}

public function specialization() {
if (isset($this->config)) {
    if (empty($this->config->title)) {
        $this->title = get_string('defaulttitle', 'block_msktime');            
    } else {
        $this->title = $this->config->title;
    }

    if (empty($this->config->text)) {
        $this->config->text = get_string('defaulttext', 'block_msktime');
    }    
}
}
public function instance_allow_multiple() {
return true;
}
function has_config() {return true;}

public function instance_config_save($data,$nolongerused =false) {
  if(get_config('msktime', 'Allow_HTML') == '1') {
    $data->text = strip_tags($data->text);
  }

  // And now forward to the default implementation defined in the parent class
  return parent::instance_config_save($data,$nolongerused);
}

}