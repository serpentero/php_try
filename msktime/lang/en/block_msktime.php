<?php
$string['pluginname'] = 'MSK Time HTML block';
$string['msktime'] = 'Moscow time';
$string['msktime:addinstance'] = 'Add a new msk time HTML block';
$string['msktime:myaddinstance'] = 'Add a new msk time HTML block to the My Moodle page';