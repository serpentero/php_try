<?php
$settings->add(new admin_setting_heading(
            'headerconfig',
            get_string('headerconfig', 'block_msktime'),
            get_string('descconfig', 'block_msktime')
        ));

$settings->add(new admin_setting_configcheckbox(
            'msktime/Allow_HTML',
            get_string('labelallowhtml', 'blo'),
            get_string('descallowhtml', 'block_msktime'),
            '0'
        ));